#!/usr/bin/env bash
#
# Mount chroot for ssh users.
#
#
# The MIT License (MIT)
#
# Copyright © 2023 metaworx resonare rüegg, Martin Rüegg, Switzerland. All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software. All redistributions must retain an intact copy of this copyright notice and disclaimer.
#
# Distributions of all or part of the Software intended to be used by the recipients as they would use the unmodified
# Software, containing modifications that substantially alter, remove, or disable functionality of the Software, outside
# of the documented configuration mechanisms provided by the Software, shall be modified such that the Original Author's
# bug reporting email addresses and urls are either replaced with the contact information of the parties responsible for
# the changes, or removed entirely.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


[ -r /usr/local/share/mwx_ssh_users/.env ] && . /usr/local/share/mwx_ssh_users/.env || { echo "$0: .env file not found!"; exit 1; }
[ -r /etc/mwx_ssh_users/.env ] && . /etc/mwx_ssh_users/.env

for i in SSH_USER_GROUP DATA_ROOT CHROOT_ROOT DEV_PATH UBIN UNION_OPT FUSE_OPT RUN_DIR LOG_DIR; do
	[ -z "${!i}" ] && echo "$0: $i is empty!" >&2 && exit 1
	export $i
done;


##
## is_integer
##
## checks if passed argument is an integer
##
is_integer()
{
	if [ "$1" == '!' ]; then
		shift
		[[ ! "$1" =~ ^-?[0-9]+$ ]]

	else
		[[ "$1" =~ ^-?[0-9]+$ ]]
	fi
}
export -f is_integer


##
## dx
##
## dx [ $return_code ... ] $command [ $argument ... ]
##
## Running $command with optional $arguments. Optional $return_code are translated to 0.
##
dx()
{
	local -i rc
	local -a RCs

	while is_integer "$1"; do
		RCs+=($1)
		shift
	done

	(( ${#RCs} == 0 )) && RCs=(0)

	(set -x; "$@")
	rc=$?
	echo "+ rc=$rc" >&2

	value="\<${rc}\>" #the value variable is assigned a regex that matches the exact value of the first argument

	if [[ ${RCs[@]} =~ $value ]]
	then
		return 0
	else
		return $rc
	fi
}
export -f dx


dmkdir()
{
	while [ -n "$1" ]; do
		[ -d "$1" ] || dx mkdir -p "$1"
		shift
	done
}
export -f dmkdir


get_sessions()
{
	w | sed '1,2d' | cut -f1 -d' ' | sort | uniq -c | sed -E 's/^[[:space:]]+//'
}
export -f get_sessions


get_user_sessions()
{
	get_sessions | grep "$1"
}
export -f get_user_sessions


get_user_sessions_count()
{
	get_user_sessions "$1" | cut -d' ' -f1
}
export -f get_user_sessions_count


create_global_dev()
{
	if ! [ -e  "$DEV_PATH/null" ]; then
		dmkdir "$DEV_PATH"

		[ -e "$DEV_PATH/null" ]    || dx mknod -m 666 "$DEV_PATH/null"    c 1 3 || return
		[ -e "$DEV_PATH/zero" ]    || dx mknod -m 666 "$DEV_PATH/zero"    c 1 5 || return
		[ -e "$DEV_PATH/full" ]    || dx mknod -m 666 "$DEV_PATH/full"    c 1 7 || return
		[ -e "$DEV_PATH/random" ]  || dx mknod -m 666 "$DEV_PATH/random"  c 1 8 || return
		[ -e "$DEV_PATH/urandom" ] || dx mknod -m 666 "$DEV_PATH/urandom" c 1 9 || return
		[ -e "$DEV_PATH/tty" ]     || dx mknod -m 666 "$DEV_PATH/tty"     c 5 0 || return
	fi
}


unionmount()
{
	local dir=$1
	local rw="$DATA_PATH/$dir"
	local un="$CHROOT_PATH/$dir"

	dmkdir "$rw"
	dmkdir "$un"

	dx $UBIN $FUSE_OPT $UNION_OPT -o "uid=$USER_ID,gid=$GROUP_ID" "${rw}=RW:/${dir}=RO" "$un"
}
export -f unionmount


bindmount()
{
	local source="${1#/}"
	local target="${2#/}"

	[ -d "$CHROOT_PATH/${target:=$source}" ] || dx dmkdir "$CHROOT_PATH/${target:=$source}"

	dx mount -o bind "/$source" "$CHROOT_PATH/$target"
}
export -f bindmount


user_mounted()
{
	local -i count=$(mount | grep "$CHROOT_ROOT/$1/" | wc -l)
	echo $count
	return $count
}
export -f user_mounted


user_mount()
{
	local -i sessions=$(get_user_sessions_count "$USER_NAME")

	echo "running user_mount for user '$USER_NAME' with $sessions active sessions"

	(( $(user_mounted "$USER_NAME") )) && return 0


	# make sure $CHROOT_PATH exists and path is only root-writeable
	#
	if ! [ -e "$CHROOT_PATH/" ]; then
		dmkdir "$CHROOT_PATH" || return

		for i in CHROOT_BASE CHROOT_ROOT CHROOT_PATH; do
			dx chmod 755 "${!i}" || return
		done
	fi


	# mount system and $HOME dirs
	#
	for i in bin lib lib64 sbin "home/$USER_NAME" run "$RUN_DIR"; do
		bindmount $i || return
	done


	# mount /usr
	#
	if ! bindmount usr; then
		for i in usr/*; do
			bindmount $i || return
		done
	fi


	# create /proc
	#
	dmkdir "$CHROOT_PATH/proc" || return
	dx mount -t proc  proc "$CHROOT_PATH/proc" || return


	# create /sys
	#
	dmkdir "$CHROOT_PATH/sys"  || return
	dx mount -t sysfs /sys "$CHROOT_PATH/sys"  || return


	# create /tmp
	#
	dmkdir "$CHROOT_PATH/tmp"  || return
	if [ -d "/run/user/$USER_ID" ]; then
		# if systemd's user temp dir exists, let's use this
		dx mount -o bind "/run/user/$USER_ID" "$CHROOT_PATH/tmp"  || return
	else
		dx mount -t tmpfs tmpfs -o "uid=$USER_ID,gid=$GROUP_ID" "$CHROOT_PATH/tmp"  || return
	fi


	# create /dev
	#
	dmkdir "$CHROOT_PATH/dev"  || return
	create_global_dev || return
	dx mount -o bind "$DEV_PATH" "$CHROOT_PATH/dev"  || return
	bindmount /dev/pts  || return


	# create /var
	#
	dmkdir "$CHROOT_PATH/var" || return
	[ -e "$CHROOT_PATH/var/run" ]  || dx ln -s /run "$CHROOT_PATH/var/run"       || return
	[ -e "$CHROOT_PATH/var/lock" ] || dx ln -s /run/lock "$CHROOT_PATH/var/lock" || return


	# create /etc
	#
	dmkdir "$CHROOT_PATH/etc" || return

	# copy passwd but exclude all real users (having "/home/" directory)
	getent passwd | grep -v /home/ > "$CHROOT_PATH/etc/passwd" || return
	# add current user
	getent passwd | grep /home/$USER_NAME >> "$CHROOT_PATH/etc/passwd" || return

	# copy groups root, nogroup, www-data, and urser's primary group to /etc/group
	getent group | grep -E '^(root|nogroup|www-data)|:x:'$GROUP_ID':' > "$CHROOT_PATH/etc/group" || return
	# add $SSH_USER_GROUP group, but with current user as only member
	echo "$(getent group $SSH_USER_GROUP | cut -d: -f1-3):$USER_NAME" >> "$CHROOT_PATH/etc/group" || return

	# copy essential /etc stuff
	dx rsync --recursive --links --times --perms --owner --group --ignore-missing-args \
		/etc/resolv.conf \
		/etc/terminfo \
		/etc/profile \
		/etc/bash.bashrc \
		/etc/profile.d \
		/etc/bash_completion.d \
		/etc/bash.bash_logout \
		/etc/manpath.config \
		/etc/alternatives \
		"$CHROOT_PATH/etc"

	# start socket so ssh_command.sh (run in user space) can log the command executed to the hosts /var/log directory
	/usr/local/sbin/ssh_log_socket.sh $USER_ID &
	
	# check host's /etc/mwx_ssh_users for additional actions
	shopt -s nullglob
	for i in /etc/mwx_ssh_users/user_mount.d/*.sh; do
		echo $BASH "'$i'" >&2
		$BASH "$i" || return
	done
	
	echo "user_mount for user '$USER_NAME' done: $(user_mounted "$USER_NAME") mounts created."
}

user_unmount()
{
	local -i sessions=$(get_user_sessions_count "$USER_NAME")

	echo "running user_unmount for user '$USER_NAME' with $sessions active sessions"

	(( sessions > 0)) && return 0

	shopt -s nullglob
	for i in /etc/mwx_ssh_users/user_unmount.d/*.sh; do
		echo $BASH "'$i'" >&2
		$BASH "$i" || return
	done

	for mp in $(mount | grep "$CHROOT_PATH/" | grep unionfs-fuse | cut -d' ' -f3 ); do
		fusermount -u "$mp";
	done

	mount | grep -E "$CHROOT_PATH"'($|/)' | cut -d' ' -f3 | LC_ALL=C sort -r | xargs --no-run-if-empty --verbose umount || echo "Error unmounting '$CHROOT_PATH'">&2

	[ -f "$RUN_DIR/$USER_ID.pid" ] && dx kill $(cat "$RUN_DIR/$USER_ID.pid") 2>/dev/null

	echo "user_unmount for user '$USER_NAME' done."
}


main()
{
	[ -z "$USER_NAME" ] && echo "user missing!">&2 && return 1


	USER_ID=$(id -u $USER_NAME)
	GROUP_ID=$(id -g $USER_NAME)
	GROUP_NAME=$(getent group $GROUP_ID | cut -d: -f1)
	export USER_ID GROUP_ID GROUP_NAME

	local mounts=$(user_mounted "$USER_NAME")

	echo $(basename "$0"): running "$1" for user "$USER_NAME ($mounts active mounts in $CHROOT_ROOT)"

	id -nG "$USER_NAME" | grep -qw $SSH_USER_GROUP || return 2

	CHROOT_PATH="$CHROOT_ROOT/$USER_NAME"
	DATA_PATH="$DATA_ROOT/$USER_NAME"
	HOME="$CHROOT_PATH/home/$USER_NAME"
	export CHROOT_PATH DATA_PATH HOME

	case "$1" in

	m|mount|open_session)
		user_mount || user_unmount
		;;

	u|umount|unmount|close_session)
		user_unmount
		;;

	e|enter)
		shift
		chroot --userspec="$USER_NAME" "$CHROOT_PATH" "$@"
		;;

	*)
		mount | grep "$CHROOT_PATH/" || echo "No mountpoints mounted" >&2
		;;

	esac
}


dmkdir "$RUN_DIR" "$LOG_DIR"


# check if the file is sourced
(return 2>/dev/null)
if [ "$?" -eq "0" ]
then
    echo "$(basename "$0") installed."
elif [ "$1" == "all" ]; then
	for USER_NAME in $(getent group | sed -E '/^'$SSH_USER_GROUP':/!d; s/^'$SSH_USER_GROUP':x:955://; s/,/ /'); do
		export USER_NAME
		main "$2"
	done
else
	USER_NAME=$1
	shift
	export USER_NAME
    main "$@"
fi
