
## Installation

```shell
cd /usr/local/share/

# if you have set-up ssh access to gitlab
git clone git@gitlab.com:metaworx/open-source/mwx_ssh_users.git

# otherwise, use this line:
#git clone git@gitlab.com:metaworx/open-source/mwx_ssh_users.git

cd mwx_ssh_users/
./install.sh
```



