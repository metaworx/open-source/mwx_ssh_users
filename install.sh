#!/usr/bin/env bash
#
# Mount chroot for ssh users.
#
#
# The MIT License (MIT)
#
# Copyright © 2023 metaworx resonare rüegg, Martin Rüegg, Switzerland. All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software. All redistributions must retain an intact copy of this copyright notice and disclaimer.
#
# Distributions of all or part of the Software intended to be used by the recipients as they would use the unmodified
# Software, containing modifications that substantially alter, remove, or disable functionality of the Software, outside
# of the documented configuration mechanisms provided by the Software, shall be modified such that the Original Author's
# bug reporting email addresses and urls are either replaced with the contact information of the parties responsible for
# the changes, or removed entirely.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


if ! which socat >/dev/null; then
	echo "socat is required. Please install or make sure it's in the \$PATH" >&2
	echo "E.g.: \$ apt install socat" >&2
	exit 1
fi

for j in bin sbin; do
	for i in $(ls $j); do
		[ -e /usr/local/$j/$i ] || ln -s /usr/local/share/mwx_ssh_users/$j/$i /usr/local/$j;
	done;
done;

mkdir -p /etc/mwx_ssh_users/user_mount.d
mkdir -p /etc/mwx_ssh_users/user_unmount.d

[ -r /usr/local/share/mwx_ssh_users/.env ] || { echo ".env file not found!"; exit 1; }

. /usr/local/share/mwx_ssh_users/.env

mkdir -p /etc/mwx_ssh_users

[ -r /etc/mwx_ssh_users/.env ] && . /etc/mwx_ssh_users/.env

for i in SSH_USER_GROUP CHROOT_BASE CHROOT_ROOT LOG_DIR; do
	[ -z "${!i}" ] && echo "$i is empty!" >&2 && exit 1
done;

mkdir -p "$RUN_DIR" "$LOG_DIR"

if ! getent group "$SSH_USER_GROUP" >/dev/null; then
	addgroup "$SSH_USER_GROUP"
fi

echo
echo "Add the following to your /etc/ssh/sshd_config and reload your ssh server"
echo
echo "-------------8<-----------------------"

cat <<EOF

Match Group ssh_users
    ChrootDirectory "$CHROOT_ROOT/%u"
  #  PermitTTY no
    X11Forwarding no
    AllowTcpForwarding no
    ForceCommand "/usr/local/bin/ssh_command.sh"

EOF

echo "-------------8<-----------------------"
echo


echo
echo "Add the following to your /etc/pam.d/sshd just before the line with the pam_selinux.so command:"
echo
echo "-------------8<-----------------------"
echo "session    required     pam_exec.so debug log=$LOG_DIR/pam.log /usr/local/sbin/ssh_pam.sh"
echo "-------------8<-----------------------"
echo


echo
echo "Add the following to your /etc/fstab:"
echo
echo "-------------8<-----------------------"
echo "tmpfs $CHROOT_BASE tmpfs defaults,mode=0755 0 0"
echo "-------------8<-----------------------"
echo
