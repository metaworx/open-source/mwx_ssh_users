#!/usr/bin/env bash
#
# Mount chroot for ssh users.
#
#
# The MIT License (MIT)
#
# Copyright © 2023 metaworx resonare rüegg, Martin Rüegg, Switzerland. All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software. All redistributions must retain an intact copy of this copyright notice and disclaimer.
#
# Distributions of all or part of the Software intended to be used by the recipients as they would use the unmodified
# Software, containing modifications that substantially alter, remove, or disable functionality of the Software, outside
# of the documented configuration mechanisms provided by the Software, shall be modified such that the Original Author's
# bug reporting email addresses and urls are either replaced with the contact information of the parties responsible for
# the changes, or removed entirely.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


[ -r /usr/local/share/mwx_ssh_users/.env ] && . /usr/local/share/mwx_ssh_users/.env || { echo ".env file not found!"; exit 1; }
[ -r /etc/mwx_ssh_users/.env ] && . /etc/mwx_ssh_users/.env

for i in RUN_DIR; do
	[ -z "${!i}" ] && echo "$i is empty!" >&2 && exit 1
done;

mkdir -p "$RUN_DIR"

unset SSH_USER_GROUP
unset DATA_ROOT
unset CHROOT_ROOT

unset LOG_DIR

unset UBIN
unset UNION_OPT
unset FUSE_OPT

if [ -n "$SSH_ORIGINAL_COMMAND" ]
then
	echo "$(/bin/date +%Y-%m-%dT%H:%M:%S%z): $SSH_ORIGINAL_COMMAND" > "$RUN_DIR/$(id -u).log"
	unset RUN_DIR

	exec $BASH -c "$SSH_ORIGINAL_COMMAND"
	exit $?
fi

echo "$(/bin/date +%Y-%m-%dT%H:%M:%S%z): login shell" > "$RUN_DIR/$(id -u).log"
unset RUN_DIR

exec /bin/bash -i
exec /bin/bash --restricted
